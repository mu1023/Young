#include "Reactor.h"
#include<Connector.h>

void Young::Reactor::PushEvent(EventFunction func)
{
	std::lock_guard<std::mutex> gd(m_QueueMutex);
	m_Queue.push(func);
}
