#ifndef _YOUNG_REACTOR_H_
#define _YOUNG_REACTOR_H_
#include <memory>
#include <mutex>
#include <queue>
#include <functional>
#include <unordered_map>
		 
#include <NetWorkAPI.h>
#include <EventHandler.h>

namespace Young
{
	class EventHandler;
	typedef std::shared_ptr<EventHandler> EventHandlerPtr;

	const UInt32 LoopEventNum = 50;

	class Reactor : public std::enable_shared_from_this<Reactor>
	{
	public:
		typedef std::function<void()> EventFunction;
		Reactor()
		{
		}

		virtual void OnTick(UInt32 now)
		{
			UInt32 cnt = LoopEventNum;

			while (cnt--)
			{
				EventFunction func;

				{
					std::lock_guard<std::mutex> gd(m_QueueMutex);
					if (!m_Queue.empty())
					{
						func = m_Queue.front();
						auto b = func;
						m_Queue.pop();
					}
					else
					{
						break;
					}
				}
				
				func();
			}

			std::vector<SocketFd> closeConns;
			for(auto iter = m_Handlers.begin();iter!=m_Handlers.end();++iter)
			{
				if (iter->second->IsClose())
				{
					//closeConns.push_back(it.second->GetFd());
				}
			}
		}
		
		virtual void UpdateHandler(EventHandlerPtr EventHandler, Events events) = 0;

		void RegisterHandler(EventHandlerPtr EventHandler, Events events){ UpdateHandler(EventHandler, events); }

		virtual void RemoveHandler(EventHandlerPtr EventHandler){ UpdateHandler(EventHandler, NoneEvent);}
	
		virtual Events ReadEvent() = 0;;

		virtual Events WriteEvent() = 0;;

		virtual Events ErrorEvent() = 0;;


		virtual void OnConnected(SocketFd sockfd) = 0;
		
		void PushEvent(EventFunction func);

	protected:

		std::unordered_map<SocketFd, EventHandlerPtr> m_Handlers;

	private:

		std::queue<std::function<void ()>>	m_Queue;
		std::mutex							m_QueueMutex;
	};
}

#endif // _YOUNG_REACTOR_H_
