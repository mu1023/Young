#ifndef _YOUNG_POOL_REACTOR_H_
#define _YOUNG_POOL_REACTOR_H_
#include <Reactor.h>
#include <EventHandler.h>

#include <vector>
#include <unordered_map>

namespace Young
{
	class PollReactor :public Reactor
	{
	public:
		typedef std::vector<struct pollfd> PollFdVec;

		PollReactor();

		~PollReactor();

		//读事件
		 Events ReadEvent()override;
		
		//写事件
		Events WriteEvent()override;
		
		//错误事件
		Events ErrorEvent()override;


		void OnTick(UInt32 timeout)override;

		//修改连接的监听事件
		void UpdateHandler(EventHandlerPtr EventHandler, Events events)override;

	private:

		PollFdVec							 m_Pollfds;

		std::unordered_map<SocketFd, UInt32> m_IndexBySocketFd;
	};

}

#endif